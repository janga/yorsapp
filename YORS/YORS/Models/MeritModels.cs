﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace YORS.Models
{
    public class MeritModels
    {
        private readonly SqlConnection _con = new SqlConnection(ConfigurationManager.ConnectionStrings["YORSDBConnection"].ConnectionString);
        private readonly SqlDataAdapter _da = new SqlDataAdapter();
        private readonly DataTable _dt = new DataTable();
        private SqlCommand _cmd = new SqlCommand();

        public MeritModels()
        {

        }
        public DataTable GetAllCategories()
        {
            try
            {
                _con.Open();
                _cmd = new SqlCommand("dbo.GetAllCategories", _con);
                _cmd.CommandType = CommandType.StoredProcedure;
                _da.SelectCommand = _cmd;
                _da.Fill(_dt);
                return _dt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                _dt.Clear();
                _cmd.Dispose();
                _con.Close();
            }
        }
        public DataTable GetAllReportGroups()
        {
            try
            {
                _con.Open();
                _cmd = new SqlCommand("dbo.GetAllReportGroups", _con);
                _cmd.CommandType = CommandType.StoredProcedure;
                _da.SelectCommand = _cmd;
                _da.Fill(_dt);
                return _dt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                _dt.Clear();
                _cmd.Dispose();
                _con.Close();
            }
        }
        public DataTable GetAllTrainingLevels()
        {
            try
            {
                _con.Open();
                _cmd = new SqlCommand("dbo.GetAllTrainingLevels", _con);
                _cmd.CommandType = CommandType.StoredProcedure;
                _da.SelectCommand = _cmd;
                _da.Fill(_dt);
                return _dt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                _dt.Clear();
                _cmd.Dispose();
                _con.Close();
            }
        }

    }
}