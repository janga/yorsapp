﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace YORS.Models
{
    public class SecurityModels
    {
        private readonly SqlConnection _con = new SqlConnection(ConfigurationManager.ConnectionStrings["YORSDBConnection"].ConnectionString);
        private readonly SqlDataAdapter _da = new SqlDataAdapter();
        private readonly DataTable _dt = new DataTable();
        private SqlCommand _cmd = new SqlCommand();

        public DataTable GetUserRole(string userId, string unitId)
        {

            try
            {
                
                userId = "cbca4e6c-87f7-436c-be34-bd3a33d4e2b1";
                unitId = "72E66108-B692-49D0-A030-7DFE7EE3B5F8";
                _con.Open();
                _cmd = new SqlCommand("dbo.GetUserRoleByUnitId", _con);
                _cmd.Parameters.Add(new SqlParameter("@UserId", userId));
                _cmd.Parameters.Add(new SqlParameter("@UnitId", unitId));
                _cmd.CommandType = CommandType.StoredProcedure;
                _da.SelectCommand = _cmd;
                _da.Fill(_dt);
                return _dt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                //_dt.Clear();
                _cmd.Dispose();
                _con.Close();
            }
        }

        public DataTable GetAvailRole(string userId, string unitId)
        {

            try
            {
                _dt.Clear();
                userId = "cbca4e6c-87f7-436c-be34-bd3a33d4e2b1";
                unitId = "72E66108-B692-49D0-A030-7DFE7EE3B5F8";
                _con.Open();
                _cmd = new SqlCommand("dbo.GetAvailRolesByUserID", _con);
                _cmd.Parameters.Add(new SqlParameter("@UserId", userId));
                _cmd.Parameters.Add(new SqlParameter("@UnitId", unitId));
                _cmd.CommandType = CommandType.StoredProcedure;
                _da.SelectCommand = _cmd;
                _da.Fill(_dt);
                return _dt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                //_dt.Clear();
                _cmd.Dispose();
                _con.Close();
            }
        }

        public string GetSelectedUnit(string userId)
        {
            try
            {
                userId = "20c0689d-cd4a-485b-9335-f8f0d0f1eebc";
                _dt.Clear();
                _con.Open();
                _cmd = new SqlCommand("dbo.GetSelectedUnitByUserId", _con);
                _cmd.Parameters.Add(new SqlParameter("@UserId", userId));
                _cmd.CommandType = CommandType.StoredProcedure;
                _da.SelectCommand = _cmd;
                _da.Fill(_dt);
                return _dt.Rows[0]["unitId"].ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                _dt.Clear();
                _cmd.Dispose();
                _con.Close();
            }
        }
    }
}