﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace YORS.Models
{
  
    public class RoleModels
    {
        private readonly SqlConnection _con = new SqlConnection(ConfigurationManager.ConnectionStrings["YORSDBConnection"].ConnectionString);
        private readonly SqlDataAdapter _da = new SqlDataAdapter();
        private readonly DataTable _dt = new DataTable();
        private SqlCommand _cmd = new SqlCommand();
        public void InsertRole(string newRole)
        {
            try
            {
                _con.Open();
                _cmd = new SqlCommand("dbo.InsertRole", _con);
                _cmd.Parameters.Add(new SqlParameter("@Id", Guid.NewGuid()));
                _cmd.Parameters.Add(new SqlParameter("@roleName", newRole));
                _cmd.CommandType = CommandType.StoredProcedure;
                _cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                _cmd.Dispose();
                _con.Close();
            }
        }

        public bool CheckRoleExists(string newRole)
        {
            var result = true;
            try
            {
                _con.Open();
                _cmd = new SqlCommand("dbo.CheckDuplicateRole", _con);
                _cmd.Parameters.Add(new SqlParameter("@roleName", newRole));
                _cmd.CommandType = CommandType.StoredProcedure;
                _da.SelectCommand = _cmd;
                _da.Fill(_dt);
                if (_dt.Rows.Count == 0)
                    result = false;
                _dt.Clear();
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                _cmd.Dispose();
                _con.Close();
            }
        }

        public string GetRoleById(string id)
        {       
            try
            {
                _con.Open();
                _cmd = new SqlCommand("dbo.GetRolesById", _con);
                _cmd.Parameters.Add(new SqlParameter("@Id", id));
                _cmd.CommandType = CommandType.StoredProcedure;
                _da.SelectCommand = _cmd;
                _da.Fill(_dt);          
                return _dt.Rows[0].ItemArray[1].ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                _dt.Clear();
                _cmd.Dispose();
                _con.Close();
            }
        }
        public void UpdateRole(string id, string name)
        {
            try
            {
                _con.Open();
                _cmd = new SqlCommand("dbo.UpdateRole", _con);
                _cmd.Parameters.Add(new SqlParameter("@Id", id));
                _cmd.Parameters.Add(new SqlParameter("@Name", name));
                _cmd.CommandType = CommandType.StoredProcedure;
                _cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                _cmd.Dispose();
                _con.Close();
            }
        }
        public void DeleteRole(string id)
        {
            try
            {
                _con.Open();
                _cmd = new SqlCommand("dbo.DeleteRoleById", _con);
                _cmd.Parameters.Add(new SqlParameter("@Id", id));
                _cmd.CommandType = CommandType.StoredProcedure;
                _cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                _cmd.Dispose();
                _con.Close();
            }
        }
    }
}