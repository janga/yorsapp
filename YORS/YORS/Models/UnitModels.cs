﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace YORS.Models
{
    public class UnitModels
    {
        private readonly SqlConnection _con = new SqlConnection(ConfigurationManager.ConnectionStrings["YORSDBConnection"].ConnectionString);
        private readonly SqlDataAdapter _da = new SqlDataAdapter();
        private readonly DataTable _dt = new DataTable();
        private SqlCommand _cmd = new SqlCommand();

        public DataTable GetUnits()
        {
            try
            {
                _con.Open();
                _cmd = new SqlCommand("dbo.GetAllUnits", _con);
                _cmd.CommandType = CommandType.StoredProcedure;
                _da.SelectCommand = _cmd;
                _da.Fill(_dt);
                return _dt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                //_dt.Clear();
                _cmd.Dispose();
                _con.Close();
            }
        }

        public DataTable GetDivisions()
        {
            try
            {
                _con.Open();
                _cmd = new SqlCommand("dbo.GetAllDivisions", _con);
                _cmd.CommandType = CommandType.StoredProcedure;
                _da.SelectCommand = _cmd;
                _da.Fill(_dt);
                return _dt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
               // _dt.Clear();
                _cmd.Dispose();
                _con.Close();
            }
        }

        public bool CheckUnitExists(string newUnit, string divisionId, string address, string phoneNo)
        {
            var result = true;
            try
            {
                _con.Open();
                _cmd = new SqlCommand("dbo.CheckDuplicateUnit", _con);
                _cmd.Parameters.Add(new SqlParameter("@unitName", newUnit));
                _cmd.Parameters.Add(new SqlParameter("@DivisionId", divisionId));
                _cmd.Parameters.Add(new SqlParameter("@Address", address));
                _cmd.Parameters.Add(new SqlParameter("@PhoneNumber", phoneNo));
                _cmd.CommandType = CommandType.StoredProcedure;
                _da.SelectCommand = _cmd;
                _da.Fill(_dt);
                if (_dt.Rows.Count == 0)
                    result = false;
                _dt.Clear();
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                _cmd.Dispose();
                _con.Close();
            }
        }

        public void InsertUnit(string newUnit, string divisionId, string address, string phoneNo)
        {
            try
            {
                _con.Open();
                _cmd = new SqlCommand("dbo.InsertUnit", _con);
                _cmd.Parameters.Add(new SqlParameter("@UnitName", newUnit));
                _cmd.Parameters.Add(new SqlParameter("@DivisionId", divisionId));
                _cmd.Parameters.Add(new SqlParameter("@Address", address));
                _cmd.Parameters.Add(new SqlParameter("@PhoneNumber", phoneNo));
                _cmd.CommandType = CommandType.StoredProcedure;
                _cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                _cmd.Dispose();
                _con.Close();
            }
        }

        public DataTable GetUnitById(string id)
        {
            try
            {
                _con.Open();
                _cmd = new SqlCommand("dbo.GetUnitById", _con);
                _cmd.Parameters.Add(new SqlParameter("@Id", id));
                _cmd.CommandType = CommandType.StoredProcedure;
                _da.SelectCommand = _cmd;
                _da.Fill(_dt);
                return _dt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                //_dt.Clear();
                _cmd.Dispose();
                _con.Close();
            }
        }

        public void UpdateUnit(string unitId, string unitName, string divisionId, string address, string phoneNo)
        {
            try
            {
                _con.Open();
                _cmd = new SqlCommand("dbo.UpdateUnit", _con);
                _cmd.Parameters.Add(new SqlParameter("@UnitId", unitId));
                _cmd.Parameters.Add(new SqlParameter("@unitName", unitName));
                _cmd.Parameters.Add(new SqlParameter("@DivisionId", divisionId));
                _cmd.Parameters.Add(new SqlParameter("@Address", address));
                _cmd.Parameters.Add(new SqlParameter("@PhoneNumber", phoneNo));
                _cmd.CommandType = CommandType.StoredProcedure;
                _cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                _cmd.Dispose();
                _con.Close();
            }
        }
        public void DeleteUnit(string id)
        {
            try
            {
                _con.Open();
                _cmd = new SqlCommand("dbo.DeleteUnitById", _con);
                _cmd.Parameters.Add(new SqlParameter("@Id", id));
                _cmd.CommandType = CommandType.StoredProcedure;
                _cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                _cmd.Dispose();
                _con.Close();
            }
        }
        public bool CheckUnitRequestExists(string unitId, string userId)
        {
            var result = true;
            try
            {
                _con.Open();
                _cmd = new SqlCommand("dbo.CheckDuplicateUnitRequest", _con);
                _cmd.Parameters.Add(new SqlParameter("@UnitId", unitId));
                _cmd.Parameters.Add(new SqlParameter("@UserId", userId));
                _cmd.CommandType = CommandType.StoredProcedure;
                _da.SelectCommand = _cmd;
                _da.Fill(_dt);
                if (_dt.Rows.Count == 0)
                    result = false;
                _dt.Clear();
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                _cmd.Dispose();
                _con.Close();
            }
        }
    }
}