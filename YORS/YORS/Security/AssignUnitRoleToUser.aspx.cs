﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using YORS.Models;

namespace YORS.Security
{
    public partial class AssignUnitRoleToUser : Page
    {
        private string unitID;
        private SecurityModels securityModel = new SecurityModels();
       // private GridView gvUserRole = new GridView();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Master.SelectedUnit != string.Empty && Master.SelectedUnit != "")
                unitID = Master.SelectedUnit;
            else
                unitID = securityModel.GetSelectedUnit(User.Identity.GetUserId());

            BindGridView();
            if (IsPostBack) return;

            gvUserRole.AllowPaging = true;
            gvUserRole.PageSize = 15;
            gvUserRole.AllowSorting = true;
            gvUserRole.RowDataBound += GvUserRole_RowDataBound;


            ViewState["SortExpression"] = "";

               // var obj = Maste
            
        }

        private void GvUserRole_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            DropDownList MoneyTypeEdit = e.Row.FindControl("DropDownList1") as DropDownList;
            e.Row.Cells[2].Visible = false;
        }

        private void BindGridView()
        {
           // var security = new SecurityModels();
           //var obj = (DropDownList)Master.FindControl("ddlSelectedUnit");
           //var str = obj.SelectedValue;
           // var obj3 = Master.SelectedUnit;

            
            gvUserRole.DataSource = securityModel.GetUserRole(User.Identity.GetUserId(), unitID);
            gvUserRole.DataBind();
            
            
        }

    }
}