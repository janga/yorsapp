﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AssignUnitRoleToUser.aspx.cs" Inherits="YORS.Security.AssignUnitRoleToUser" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:GridView ID="gvUserRole" runat="server" AutoGenerateColumns="false" onrowdatabound="GvUserRole_RowDataBound">
        <Columns>
            <asp:BoundField HeaderText="userID" DataField="userID" Visible="false" />
            <asp:BoundField HeaderText="User Name" DataField="userName" />
            <asp:BoundField HeaderText="Role Name" DataField="roleName" />
            <asp:BoundField HeaderText="roleID" DataField="roleID" Visible="false" />
            <asp:TemplateField HeaderText="Role Name" SortExpression="Role Name">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownList1" runat="server" 
                        DataSourceID="SqlDataSource2" DataTextField="roleID" DataValueField="roleName" 
                        SelectedValue='<%# Bind("roleName") %>'  >
                    </asp:DropDownList>
                </EditItemTemplate>
                <ItemTemplate>
                <%--    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Role") %>'></asp:Label>--%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>

