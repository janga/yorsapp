--Name : DeleteUnitById
--Written by : Andrew Jang
--Purpose : 
--Log
--Andrew Jang - Created - March 16th, 2017

use DB_110913_yors

if OBJECT_ID('DeleteUnitById') IS NOT NULL
  begin
	drop procedure DeleteUnitById
  end
go

Create procedure DeleteUnitById
	@Id nvarchar(128)
as

begin
begin tran
SET NOCOUNT ON;

delete from Unit
where UnitId = @Id

--rollback tran
commit tran
end
go