--Name : GetAvailableUnits
--Written by : Andrew Jang
--Purpose : Retrieve List of available units
--Log
--Andrew Jang - Created - March 15th, 2017

use DB_110913_yors

if OBJECT_ID('GetAvailableUnits') IS NOT NULL
  begin
	drop procedure GetAvailableUnits
  end
go

Create procedure GetAvailableUnits
	@UserId as nvarchar(128)
as

begin
begin tran
SET NOCOUNT ON;

select u.* 
into #tmpAvailUnits
from  Unit u left join User_Unit uu on uu.UnitId = u.UnitId and uu.UserId = @UserId 
where uu.UnitId is null

select au.* 
from #tmpAvailUnits au left join User_Unit_Request uur on au.UnitId = uur.UnitId and uur.UserId = @UserId 
where uur.UnitId is null

--rollback tran
commit tran
end
go