--Name : GetUserRoleByUnitId
--Written by : Andrew Jang
--Purpose : 
--Log
--Andrew Jang - Created - March 31st, 2017

use DB_110913_yors

if OBJECT_ID('GetUserRoleByUnitId') IS NOT NULL
  begin
	drop procedure GetUserRoleByUnitId
  end
go

Create procedure GetUserRoleByUnitId
	@UnitId uniqueidentifier,
	@UserId nvarchar(128)
as

begin
begin tran
SET NOCOUNT ON;

declare @Role as nvarchar(128)

select @Role = r.[Name] from AspNetUserRoles ur inner join AspNetRoles r on ur.RoleId = r.Id
where ur.UnitId = @UnitId and ur.UserId = @UserId

if @Role = 'System Admin'
 begin
	select u.Id as userID, u.FirstName + ' ' + u.LastName as userName, r.Id as roleID, r.[Name] as roleName
	from AspNetUsers u inner join AspNetUserRoles ur on ur.UserId = u.Id
					   inner join AspNetRoles r on r.Id = ur.RoleId
	where ur.UnitId = @UnitId and u.id <> @UserId
 end

if @Role = 'Site Admin'
 begin
	select u.Id as userID, u.FirstName + ' ' + u.LastName as userName, r.Id as roleID, r.[Name] as roleName
	from AspNetUsers u inner join AspNetUserRoles ur on ur.UserId = u.Id
					   inner join AspNetRoles r on r.Id = ur.RoleId
	where ur.UnitId = @UnitId and r.[Name] <> 'System Admin' and u.id <> @UserId
 end

if @Role = 'Staff'
 begin
	select u.Id as userID, u.FirstName + ' ' + u.LastName as userName, r.Id as roleID, r.[Name] as roleName
	from AspNetUsers u inner join AspNetUserRoles ur on ur.UserId = u.Id
					   inner join AspNetRoles r on r.Id = ur.RoleId
	where ur.UnitId = @UnitId and r.[Name] not in ('System Admin', 'Site Admin') and u.id <> @UserId
 end

 if @Role not in ('Staff', 'Site Admin', 'System Admin')
  begin
	select '' as userID, '' as userName, '' as roleID, '' as roleName
  end

--select u.Id as userID, u.FirstName + ' ' + u.LastName as userName 
--from AspNetUsers u inner join AspNetUserRoles ur on ur.UserId = u.Id
--				   inner join AspNetRoles r on r.Id = ur.RoleId
--where ur.UnitId = @UnitId

--rollback tran
commit tran
end
go