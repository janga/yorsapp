--Name : GetRolesById
--Written by : Andrew Jang
--Purpose : 
--Log
--Andrew Jang - Created - March 16th, 2017

use DB_110913_yors

if OBJECT_ID('GetRolesById') IS NOT NULL
  begin
	drop procedure GetRolesById
  end
go

Create procedure GetRolesById
	@Id nvarchar(128)
as

begin
begin tran
SET NOCOUNT ON;

select Id, Name from AspNetRoles where id = @Id

--rollback tran
commit tran
end
go