--Name : GetRoles
--Written by : Andrew Jang
--Purpose : 
--Log
--Andrew Jang - Created - March 16th, 2017

use DB_110913_yors

if OBJECT_ID('GetRoles') IS NOT NULL
  begin
	drop procedure GetRoles
  end
go

Create procedure GetRoles
as

begin
begin tran
SET NOCOUNT ON;

select Id, Name from AspNetRoles

--rollback tran
commit tran
end
go