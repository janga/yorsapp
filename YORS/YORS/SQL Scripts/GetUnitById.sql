--Name : GetUnitById
--Written by : Andrew Jang
--Purpose : 
--Log
--Andrew Jang - Created - March 16th, 2017

use DB_110913_yors

if OBJECT_ID('GetUnitById') IS NOT NULL
  begin
	drop procedure GetUnitById
  end
go

Create procedure GetUnitById
	@Id uniqueidentifier
as

begin
begin tran
SET NOCOUNT ON;

select UnitId, DivisionId, UnitName, Address, PhoneNumber from Unit where Unitid = @Id

--rollback tran
commit tran
end
go