--Name : GetAllUnits
--Written by : Andrew Jang
--Purpose : 
--Log
--Andrew Jang - Created - March 16th, 2017

use DB_110913_yors

if OBJECT_ID('GetAllUnits') IS NOT NULL
  begin
	drop procedure GetAllUnits
  end
go

Create procedure GetAllUnits
as

begin
begin tran
SET NOCOUNT ON;

select UnitId, DivisionId, UnitName from Unit

--rollback tran
commit tran
end
go