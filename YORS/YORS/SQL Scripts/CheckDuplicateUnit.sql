--Name : CheckDuplicateUnit
--Written by : Andrew Jang
--Purpose : 
--Log
--Andrew Jang - Created - March 16th, 2017

use DB_110913_yors

if OBJECT_ID('CheckDuplicateUnit') IS NOT NULL
  begin
	drop procedure CheckDuplicateUnit
  end
go

Create procedure CheckDuplicateUnit
	@unitName as nvarchar(50),
	@DivisionId as uniqueidentifier,
	@Address as nvarchar(50),
	@PhoneNumber as nvarchar(12)
as

begin
begin tran
SET NOCOUNT ON;

select * from Unit where [UnitName] = @unitName and DivisionId = @DivisionId and PhoneNumber = @PhoneNumber and Address = @Address
--rollback tran
commit tran
end
go