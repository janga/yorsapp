--Name : CheckDuplicateRole
--Written by : Andrew Jang
--Purpose : Checks if there is a duplicate role
--Log
--Andrew Jang - Created - March 16th, 2017

use DB_110913_yors

if OBJECT_ID('CheckDuplicateRole') IS NOT NULL
  begin
	drop procedure CheckDuplicateRole
  end
go

Create procedure CheckDuplicateRole
	@roleName as nvarchar(128)
as

begin
begin tran
SET NOCOUNT ON;

select * from AspNetRoles where [Name] = @roleName
--rollback tran
commit tran
end
go