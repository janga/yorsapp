--Name : GetUnitNameById
--Written by : Andrew Jang
--Purpose : 
--Log
--Andrew Jang - Created - March 16th, 2017

use DB_110913_yors

if OBJECT_ID('GetUnitNameById') IS NOT NULL
  begin
	drop procedure GetUnitNameById
  end
go

Create procedure GetUnitNameById
	@Id uniqueidentifier
as

begin
begin tran
SET NOCOUNT ON;

select UnitId, DivisionId, UnitName from Unit where Unitid = @Id

--rollback tran
commit tran
end
go