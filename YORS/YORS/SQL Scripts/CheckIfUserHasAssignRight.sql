--Name : CheckIfUserHasAssignRight
--Written by : Andrew Jang
--Purpose : 
--Log
--Andrew Jang - Created - March 17th, 2017

use DB_110913_yors

if OBJECT_ID('CheckIfUserHasAssignRight') IS NOT NULL
  begin
	drop procedure CheckIfUserHasAssignRight
  end
go

Create procedure CheckIfUserHasAssignRight
	@UnitId uniqueidentifier,
	@UserId nvarchar(128)
as

begin
begin tran
SET NOCOUNT ON;

if exists(select top 1 * from AspNetUserRoles ur inner join AspNetRoles r on ur.RoleId = r.Id where ur.UserId = @UserId and r.Name = 'System Admin' )
  begin
	select 'true' as Result
	return
  end

if exists(select top 1 * from AspNetUserRoles ur inner join AspNetRoles r on ur.RoleId = r.Id where ur.UserId = @UserId and r.Name = 'Site Admin' )
  begin
	select 'true' as Result
	return
  end
--select u.Id as userID, u.FirstName + ' ' + u.LastName as userName 
--from AspNetUsers u inner join AspNetUserRoles ur on ur.UserId = u.Id
--				   inner join AspNetRoles r on r.Id = ur.RoleId
--where ur.UnitId = @UnitId and ur.UserId = @UserId and  r.Name = 'SiteAdmin'
select 'false' as Result
return

--rollback tran
commit tran
end
go