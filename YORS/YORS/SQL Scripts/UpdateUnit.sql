--Name : UpdateUnit
--Written by : Andrew Jang
--Purpose : 
--Log
--Andrew Jang - Created - March 16th, 2017

use DB_110913_yors

if OBJECT_ID('UpdateUnit') IS NOT NULL
  begin
	drop procedure UpdateUnit
  end
go

Create procedure UpdateUnit
	@UnitId uniqueidentifier,
	@UnitName as nvarchar(50),
	@DivisionId as uniqueidentifier,
	@Address as nvarchar(50) null,
	@PhoneNumber as nvarchar(12) null
as

begin
begin tran
SET NOCOUNT ON;

update Unit
   set UnitName = @UnitName, Address = @Address, PhoneNumber = @PhoneNumber
 where UnitId = @UnitId and DivisionId = @DivisionId

--rollback tran
commit tran
end
go