--Name : UpdateRole
--Written by : Andrew Jang
--Purpose : 
--Log
--Andrew Jang - Created - March 16th, 2017

use DB_110913_yors

if OBJECT_ID('UpdateRole') IS NOT NULL
  begin
	drop procedure UpdateRole
  end
go

Create procedure UpdateRole
	@Id nvarchar(128),
	@Name nvarchar(256)
as

begin
begin tran
SET NOCOUNT ON;

update AspNetRoles
   set [Name] = @Name
 where Id = @Id

--rollback tran
commit tran
end
go