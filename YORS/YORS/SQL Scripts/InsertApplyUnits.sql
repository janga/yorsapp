--Name : InsertApplyUnits
--Written by : Andrew Jang
--Purpose : Copy data from Units to User_units
--Log
--Andrew Jang - Created - March 15th, 2017

use DB_110913_yors

if OBJECT_ID('InsertApplyUnits') IS NOT NULL
  begin
	drop procedure InsertApplyUnits
  end
go

Create procedure InsertApplyUnits
	@UserId as nvarchar(128),
	@UnitId as uniqueidentifier
as

begin
begin tran
SET NOCOUNT ON;

insert into User_Unit_Request (UserId, UnitId, ModifiedByUserId, ModifiedDate, CreatedByUserId, CreatedDate)
select @UserId, @UnitId, @UserId, GETDATE(), @UserId, GETDATE()

--rollback tran
commit tran
end
go