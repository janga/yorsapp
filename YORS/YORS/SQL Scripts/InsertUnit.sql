--Name : InsertUnit
--Written by : Andrew Jang
--Purpose : 
--Log
--Andrew Jang - Created - March 16th, 2017

use DB_110913_yors

if OBJECT_ID('InsertUnit') IS NOT NULL
  begin
	drop procedure InsertUnit
  end
go

Create procedure InsertUnit
	@UnitName as nvarchar(50),
	@DivisionId as uniqueidentifier,
	@Address as nvarchar(50) null,
	@PhoneNumber as nvarchar(12) null
as

begin
begin tran
SET NOCOUNT ON;

insert into Unit ([UnitName], DivisionId, [Address], [PhoneNumber], ModifiedByUserId, ModifiedDate, CreatedByUserId, CreatedDate)
select @UnitName, @DivisionId, @Address, @PhoneNumber, CURRENT_USER, GETDATE(), CURRENT_USER, GETDATE()

--rollback tran
commit tran
end
go