--Name : GetSelectedUnitByUserId
--Written by : Andrew Jang
--Purpose : 
--Log
--Andrew Jang - Created - April 1st, 2017

use DB_110913_yors

if OBJECT_ID('GetSelectedUnitByUserId') IS NOT NULL
  begin
	drop procedure GetSelectedUnitByUserId
  end
go

Create procedure GetSelectedUnitByUserId
	@userId nvarchar(128)
as

begin
begin tran
SET NOCOUNT ON;

select unitId from User_Unit where UserId = @userId

--rollback tran
commit tran
end
go