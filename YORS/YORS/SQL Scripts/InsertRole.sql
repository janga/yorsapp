--Name : InsertRole
--Written by : Andrew Jang
--Purpose : 
--Log
--Andrew Jang - Created - March 16th, 2017

use DB_110913_yors

if OBJECT_ID('InsertRole') IS NOT NULL
  begin
	drop procedure InsertRole
  end
go

Create procedure InsertRole
	@Id as nvarchar(128),
	@roleName as nvarchar(256)
as

begin
begin tran
SET NOCOUNT ON;

insert into AspNetRoles (Id, [Name])
select @Id, @roleName

--rollback tran
commit tran
end
go