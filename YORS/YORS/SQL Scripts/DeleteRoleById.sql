--Name : DeleteRoleById
--Written by : Andrew Jang
--Purpose : 
--Log
--Andrew Jang - Created - March 16th, 2017

use DB_110913_yors

if OBJECT_ID('DeleteRoleById') IS NOT NULL
  begin
	drop procedure DeleteRoleById
  end
go

Create procedure DeleteRoleById
	@Id nvarchar(128)
as

begin
begin tran
SET NOCOUNT ON;

delete from AspNetRoles
where Id = @Id

--rollback tran
commit tran
end
go