--Name : GetAllDivisions
--Written by : Andrew Jang
--Purpose : 
--Log
--Andrew Jang - Created - March 16th, 2017

use DB_110913_yors

if OBJECT_ID('GetAllDivisions') IS NOT NULL
  begin
	drop procedure GetAllDivisions
  end
go

Create procedure GetAllDivisions
as

begin
begin tran
SET NOCOUNT ON;

select DivisionId, [Name] from Division

--rollback tran
commit tran
end
go