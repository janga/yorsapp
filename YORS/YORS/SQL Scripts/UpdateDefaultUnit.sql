--Name : UpdateDefaultUnit
--Written by : Andrew Jang
--Purpose : 
--Log
--Andrew Jang - Created - March 15th, 2017

use DB_110913_yors

if OBJECT_ID('UpdateDefaultUnit') IS NOT NULL
  begin
	drop procedure UpdateDefaultUnit
  end
go

Create procedure UpdateDefaultUnit
	@UserId as nvarchar(128),
	@UnitId as uniqueidentifier
as

begin
begin tran
SET NOCOUNT ON;

update User_Unit
   set isDefault = 0
 where userId = @UserId

update User_Unit
   set isDefault = 1
 where UserId = @UserId
   and UnitId = @UnitId
--rollback tran
commit tran
end
go