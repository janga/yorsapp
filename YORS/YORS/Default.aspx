﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="YORS._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Under Construction</h1>
        <p class="lead">
            This website provides youth organizations with the ability to reward their membership for participation in their events. 
            The system will grow to provide additional services outside of purely merit. Over time, this will be a one-stop shop for all non-profit groups!
        </p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Merit System</h2>
            <p>
                Earn points for regular meeting attendance. Also earn points for attending special events and activities. 
                Increase your points by planning in advance, attending activities and events, helping the organizers by volunteering as staff, and by being involved. 
            </p>
        </div>
        <div class="col-md-4">
            <h2>Calendar System</h2>
            <p>
                Modernize your calendar of events by having everything online and available from anywhere in the world. 
                Let your organization's youth sign up for your events, track where they seem to be most interested, 
                and keep track of their participation as this system ties in closely with the Merit System. 
                Allow your adult volunteers to register for events so they can confirm that they are available to assist and 
                since this ties into the Sponsor System, provide them with rewards too. Ensure that your events are given the best chance to succeed.
            </p>
        </div>
        <div class="col-md-4">
            <h2>Sponsor System</h2>
            <p>
                Have your adult volunteers, the ones who are helping to keep your organization moving forward, 
                register themselves using the self-register kiosk. Then track their participation in the organization to 
                show which of your volunteers are truly helping with the operations. Reward your volunteers for their efforts. 
                Let them know you appreciate them.
            </p>
        </div>
    </div>

</asp:Content>
