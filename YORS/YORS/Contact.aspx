﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="YORS.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h3>YORS Contact List</h3>
    <address>
        Richmond, BC<br />
    </address>

    <address>
        <strong>Sales:</strong>   <a href="mailto:jglaisher@yors.ca">jglaisher@yors.ca</a><br />
        <strong>Support:</strong> <a href="mailto:ajang@yors.ca">ajang@yors.ca</a>
    </address>
</asp:Content>
