--Name : GetConfirmedUnits
--Written by : Andrew Jang
--Purpose : Retrieve List of confirmed units
--Log
--Andrew Jang - Created - March 15th, 2017

use DB_110913_yors

if OBJECT_ID('GetConfirmedUnits') IS NOT NULL
  begin
	drop procedure GetConfirmedUnits
  end
go

Create procedure GetConfirmedUnits
	@UserId as nvarchar(128)
as

begin
begin tran
SET NOCOUNT ON;

select u.*, uu.isDefault
from User_Unit uu inner join Unit u on uu.UnitId = u.UnitId
where uu.UserId = @UserId

--rollback tran
commit tran
end
go