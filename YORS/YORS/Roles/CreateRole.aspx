﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreateRole.aspx.cs" Inherits="YORS.Roles.CreateRole" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HyperLink runat="server" ID="ManageRole" ViewStateMode="Disabled">Goback to Manage Role</asp:HyperLink>
    <p>New role : <asp:TextBox runat="server" ID="newRole" TextMode="SingleLine"></asp:TextBox></p>
    <asp:Button runat="server" Text="Create" ID="CreateNewRole" OnClick="CreateNewRole_OnClick"/>
</asp:Content>
