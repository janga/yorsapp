﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using YORS.Models;

namespace YORS.Roles
{
    public partial class EditRole : System.Web.UI.Page
    {
        private readonly RoleModels _model = new RoleModels();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;

            editRole.Text = _model.GetRoleById(Request["Id"]);
        }

        protected void btnEditRole_OnClick(object sender, EventArgs e)
        {
            if (_model.CheckRoleExists(editRole.Text))
                Response.Write("<script>alert('Role already exists');</script>");
            else
            {
                _model.UpdateRole(Request["Id"], editRole.Text);
                Response.Redirect("~/Roles/ManageRoles");
            }
        }
    }
}