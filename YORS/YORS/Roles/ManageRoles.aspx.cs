﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;


namespace YORS.Roles
{
    public partial class ManageRoles : System.Web.UI.Page
    {
        private SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["YORSDBConnection"].ConnectionString);
        private SqlDataAdapter da = new SqlDataAdapter();
        private DataTable dt = new DataTable();
        private SqlCommand cmd = new SqlCommand();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                cmd = new SqlCommand("dbo.GetRoles", con) {CommandType = CommandType.StoredProcedure};
                da.SelectCommand = cmd;
                da.Fill(dt);

                var tableString = dt.AsEnumerable().Aggregate("<table><tr><td>Role</td><td></td><td></td></tr>", (current, item) => current + "<tr><td>" + item[1] 
                + "<td><td>"+ "<a runat='server' href='EditRole.aspx?id="+ item[0] + "'>Edit</a>" + "</td><td>" + "<a runat='server' href='DeleteRole.aspx?id=" + item[0] + "'>Delete</a>" + "</td></tr>");

                tableString = tableString + "</table>";
                roleList.InnerHtml = tableString;
                CreateRole.NavigateUrl = "CreateRole";
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
            finally
            {
                dt.Clear();
                cmd.Dispose();
            }
            
        }
    }
}