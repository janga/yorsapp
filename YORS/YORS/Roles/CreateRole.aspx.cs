﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using YORS.Models;

namespace YORS.Roles
{
    public partial class CreateRole : System.Web.UI.Page
    {
        private readonly RoleModels _model = new RoleModels();

        protected void Page_Load(object sender, EventArgs e)
        {
            ManageRole.NavigateUrl = "ManageRoles";
        }

        protected void CreateNewRole_OnClick(object sender, EventArgs e)
        {
            if (_model.CheckRoleExists(newRole.Text))
                Response.Write("<script>alert('Role already exists');</script>");
            else
            {
                _model.InsertRole(newRole.Text);
                Response.Redirect("~/Roles/ManageRoles");
            }
                
        }
    }
}