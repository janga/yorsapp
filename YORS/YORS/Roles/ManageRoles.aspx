﻿<%@ Page Title="Manage Roles" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageRoles.aspx.cs" Inherits="YORS.Roles.ManageRoles" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="roleList" runat="server"></div>
    <p>
        <asp:HyperLink runat="server" ID="CreateRole" ViewStateMode="Disabled">Create Role</asp:HyperLink>                   
    </p>
</asp:Content>
