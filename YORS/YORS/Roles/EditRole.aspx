﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditRole.aspx.cs" Inherits="YORS.Roles.EditRole" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HyperLink runat="server" ID="ManageRole" ViewStateMode="Disabled">Goback to Manage Role</asp:HyperLink>
    <p>Edit role : <asp:TextBox runat="server" ID="editRole" TextMode="SingleLine" /> </p>
    <asp:Button runat="server" Text="Save" ID="btnEditRole" OnClick="btnEditRole_OnClick"/>
</asp:Content>
