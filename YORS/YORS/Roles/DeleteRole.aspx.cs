﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using YORS.Models;

namespace YORS.Roles
{
    public partial class DeleteRole : System.Web.UI.Page
    {
        private readonly RoleModels _model = new RoleModels();
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnAgree_OnClick(object sender, EventArgs e)
        {
            _model.DeleteRole(Request["Id"]);
            Response.Redirect("~/Roles/ManageRoles");
        }

        protected void btnDisagree_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/Roles/ManageRoles");
        }
    }
}