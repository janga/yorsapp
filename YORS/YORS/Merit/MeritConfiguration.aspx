﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MeritConfiguration.aspx.cs" Inherits="YORS.Merit.MeritConfiguration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="divCategoryTitle">
        <asp:Label text="Category Configuration" runat="server"/>
    </div>
    <div id="divCateogry">
        <asp:Label Text="Category:" runat="server"/>
        <asp:TextBox ID="txtCategory" runat="server" />
    </div>
    <div id="divPoints">
        <asp:Label Text="Points:" runat="server"/>
        <asp:TextBox ID="txtPoints" runat="server" />
    </div>
    <div id="divFeatured">
        <asp:Label Text="Featured:" runat="server"/>
        <asp:CheckBox ID="cbFeatured" runat="server" />
    </div>
    <div id="divStaffOr">
        <asp:Label Text="Staff Override:" runat="server"/>
        <asp:CheckBox ID="cbStaffOr" runat="server" />
    </div>
    <div id="divCatDescr">
        <asp:Label Text="Description" runat="server" />
        <asp:TextBox ID="txtCatDescr" runat="server" TextMode="MultiLine" />
    </div>
    <div id="divCategoriesGrid">
        <asp:GridView ID="gridCategories" runat="server"/>
    </div>

    <br />

    <div id="divReportGroupsTitle">
        <asp:Label Text="Report Groups Configuration" runat="server" />
    </div>
    <div id="divReportGroupsGrid">
        <asp:GridView ID="gridReportGroups" runat="server"/>
    </div>

    <br />

    <div id="divTrainingLevelTitle">
        <asp:Label Text="Training Level Configuration" runat="server" />
    </div>
    <div id="divTrainingLevelsGrid">
        <asp:GridView ID="gridTrainingLevels" runat="server"/>
    </div>
    
</asp:Content>
