﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using YORS.Models;

namespace YORS.Merit
{
    public partial class MeritConfiguration : System.Web.UI.Page
    {
        private readonly MeritModels mm = new MeritModels();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                //getAllCategories();
                //getAllReportGroups();
                //getAllTrainingLevels();
            }

        }

        private void getAllCategories()
        {
            gridCategories.DataSource = mm.GetAllCategories();
            gridCategories.DataBind();
        }
        private void getAllReportGroups()
        {
            gridReportGroups.DataSource = mm.GetAllReportGroups();
            gridCategories.DataBind();
        }
        private void getAllTrainingLevels()
        {
            gridTrainingLevels.DataSource = mm.GetAllTrainingLevels();
            gridCategories.DataBind();
        }
    }
}