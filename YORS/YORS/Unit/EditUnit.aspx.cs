﻿using System;
using YORS.Models;

namespace YORS.Unit
{
    public partial class EditUnit : System.Web.UI.Page
    {
        private readonly UnitModels _model = new UnitModels();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
        
            ManageUnit.NavigateUrl = "ManageUnits";
            var dt = _model.GetUnitById(Request["Id"]);
            txtUnit.Text = dt.Rows[0].ItemArray[2].ToString();
            txtAddress.Text = dt.Rows[0].ItemArray[3].ToString();
            txtPhone.Text = dt.Rows[0].ItemArray[4].ToString();
            dt.Clear();
            dt = _model.GetDivisions();

            ddlSelectedDivision.DataSource = dt;
            ddlSelectedDivision.DataTextField = "Name";
            ddlSelectedDivision.DataValueField = "DivisionId";
            ddlSelectedDivision.DataBind();
            dt.Clear();
        }

        protected void btnUnit_OnClick(object sender, EventArgs e)
        {
            if (_model.CheckUnitExists(txtUnit.Text, ddlSelectedDivision.SelectedValue, txtAddress.Text, txtPhone.Text))
                Response.Write("<script>alert('Unit already exists');</script>");
            else
            {
                _model.UpdateUnit(Request["Id"], txtUnit.Text, ddlSelectedDivision.SelectedValue, txtAddress.Text, txtPhone.Text);
                Response.Redirect("~/Unit/ManageUnits");
            }
        }
    }
}