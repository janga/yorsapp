﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using YORS.Models;

namespace YORS.Unit
{
    public partial class DeleteUnit : System.Web.UI.Page
    {
        private readonly UnitModels _model = new UnitModels();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAgree_OnClick(object sender, EventArgs e)
        {
            _model.DeleteUnit(Request["Id"]);
            Response.Redirect("~/Unit/ManageUnits");
        }

        protected void btnDisagree_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/Unit/ManageUnits");
        }
    }
}