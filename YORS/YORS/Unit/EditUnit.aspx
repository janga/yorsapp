﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditUnit.aspx.cs" Inherits="YORS.Unit.EditUnit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <asp:HyperLink runat="server" ID="ManageUnit" ViewStateMode="Disabled">Goback to Manage Unit</asp:HyperLink>
    <p>Edit unit name : <asp:TextBox runat="server" ID="txtUnit" TextMode="SingleLine"></asp:TextBox> Division : <asp:DropDownList ID="ddlSelectedDivision" runat="server" /></p>
    <p>Address : <asp:TextBox runat="server" ID="txtAddress" TextMode="SingleLine" /> Phone Number : <asp:TextBox runat="server" ID="txtPhone" TextMode="Phone" /></p>
    <asp:Button runat="server" Text="Save" ID="btnUnit" OnClick="btnUnit_OnClick"/>
</asp:Content>
