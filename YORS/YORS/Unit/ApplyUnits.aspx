﻿<%@  Page Title="Apply Units" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ApplyUnits.aspx.cs" Inherits="YORS.Unit.ApplyUnits"  %>
<%@ MasterType VirtualPath="~/Site.Master" %>

<%--<!DOCTYPE html>--%>

<%--<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>--%>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<%--    <form id="form1" runat="server">--%>
<!--    <div class ="form-horizontal"> -->

    <div class="row">
        <div class="col-md-4 text-center">
            AVAILABLE UNITS
        </div>
        <div class="col-md-2">
        </div>
        <div class="col-md-3 text-center">
            REQUESTED UNITS
        </div>
        <div class="col-md-3 text-center">
            MEMBER OF UNITS
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <asp:ListBox class="listBoxSize" ID="listUnitAvailable" runat="server"></asp:ListBox>
        </div>
        <div class="col-md-2 text-center">
            <asp:Button ID="btnApply" runat="server" Text="Request Access" OnClick="btnApply_Click" OnClientClick="return confirm('Are you sure you want to apply this unit?')"/>
        </div>
        <div class="col-md-3">
            <asp:ListBox class="listBoxSize" ID="listUnitApplied" runat="server"></asp:ListBox>
        </div>
        <div class="col-md-3">
            <asp:ListBox class="listBoxSize" ID="listUnitConfirmed" runat="server"></asp:ListBox>
        </div>
    </div>

<!--    </div> -->
    <asp:HiddenField runat="server" id="hdnUnit" value=""/>
<%--    </form>--%>
</asp:Content>
<%--</html>--%>
