﻿<%@ Page Title="Create Unit" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreateUnit.aspx.cs" Inherits="YORS.Unit.CreateUnit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HyperLink runat="server" ID="ManageUnit" ViewStateMode="Disabled">Goback to Manage Unite</asp:HyperLink>
    <p>New Unit : <asp:TextBox runat="server" ID="newUnit" TextMode="SingleLine" /> Division : <asp:DropDownList ID="ddlSelectedDivision" runat="server" /></p>
    <p>Address : <asp:TextBox runat="server" ID="txtAddress" TextMode="SingleLine" /> Phone Number : <asp:TextBox runat="server" ID="txtPhone" TextMode="Phone" /></p>
    <asp:Button runat="server" Text="Create" ID="btnNewUnit" OnClick="btnNewUnit_OnClick"/>
</asp:Content>
