﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using YORS.Models;

namespace YORS.Unit
{
    public partial class CreateUnit : System.Web.UI.Page
    {
        private readonly UnitModels _model = new UnitModels();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;
            ManageUnit.NavigateUrl = "ManageUnits";
            var dt = _model.GetDivisions();

            ddlSelectedDivision.DataSource = dt;
            ddlSelectedDivision.DataTextField = "Name";
            ddlSelectedDivision.DataValueField = "DivisionId";
            ddlSelectedDivision.DataBind();
            dt.Clear();
        }

        protected void btnNewUnit_OnClick(object sender, EventArgs e)
        {
            if (_model.CheckUnitExists(newUnit.Text, ddlSelectedDivision.SelectedValue, txtAddress.Text, txtPhone.Text))
                Response.Write("<script>alert('Unit already exists');</script>");
            else
            {
                _model.InsertUnit(newUnit.Text, ddlSelectedDivision.SelectedValue, txtAddress.Text, txtPhone.Text);
                Response.Redirect("~/Unit/ManageUnits");
            }
        }
    }
}