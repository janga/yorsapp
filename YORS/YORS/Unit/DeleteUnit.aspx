﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeleteUnit.aspx.cs" Inherits="YORS.Unit.DeleteUnit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        Are you sure you want to delete this Unit?
        <asp:Button ID = "btnAgree" runat="server" OnClick="btnAgree_OnClick" Text="Yes"/>
        <asp:Button ID = "btnDisagree" runat="server" OnClick="btnDisagree_OnClick" Text="No"/>

    </div>
    </form>
</body>
</html>
