﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using YORS.Models;

namespace YORS.Unit
{
    public partial class ApplyUnits : Page
    {
        private SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["YORSDBConnection"].ConnectionString);
        private SqlDataAdapter da = new SqlDataAdapter();
        private DataTable dt = new DataTable();
        private SqlCommand cmd = new SqlCommand();

        private readonly UnitModels _model = new UnitModels();

        protected void Page_Load(object sender, EventArgs e)
        {
           try
            {
                if (!User.Identity.IsAuthenticated)
                    Response.Redirect("~/");

                if (IsPostBack) return;            
                LoadLists();

                var obj3 = Master.SelectedUnit;

                var obj = (DropDownList)Master.FindControl("ddlSelectedUnit");
                var str = obj.SelectedValue;
                var obj2 = (HtmlAnchor)Master.FindControl("unitName");
                //str = obj.Text;
                //var obj3 = ((SiteMaster)this.Master).passList;
                //str = Session["selectedUnit"].ToString();

                //var str = Master.passList;
                //var dropdown = (HiddenField)Master.FindControl("selectedUnitField");
                //var str = dropdown.Value;
                //str = hdnUnit.Value;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            
        }

        private void LoadLists()
        {
            try
            {
                con.Open();
                PopulateUnitAvailableList();
                PopulateUnitAppliedList();
                PopulateUnitConfirmedList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
            }        
        }

        private void PopulateUnitAvailableList()
        {
            cmd = new SqlCommand("dbo.GetAvailableUnits", con);
            cmd.Parameters.Add(new SqlParameter("@UserId", User.Identity.GetUserId()));
            cmd.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = cmd;
            da.Fill(dt);
            listUnitAvailable.DataSource = dt;
            listUnitAvailable.DataTextField = "UnitName";
            listUnitAvailable.DataValueField = "UnitId";
            listUnitAvailable.DataBind();
            dt.Clear();
            cmd.Dispose();
        }

        private void PopulateUnitAppliedList()
        {
            cmd = new SqlCommand("dbo.GetRequestedUnits", con);
            cmd.Parameters.Add(new SqlParameter("@UserId", User.Identity.GetUserId()));
            cmd.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = cmd;
            da.Fill(dt);
            listUnitApplied.DataSource = dt;
            listUnitApplied.DataTextField = "UnitName";
            listUnitApplied.DataValueField = "UnitId";
            listUnitApplied.DataBind();
            dt.Clear();
            cmd.Dispose();
        }

        private void PopulateUnitConfirmedList()
        {
            cmd = new SqlCommand("dbo.GetConfirmedUnits", con);
            cmd.Parameters.Add(new SqlParameter("@UserId", User.Identity.GetUserId()));
            cmd.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = cmd;
            da.Fill(dt);
            listUnitConfirmed.DataSource = dt;
            listUnitConfirmed.DataTextField = "UnitName";
            listUnitConfirmed.DataValueField = "UnitId";
            listUnitConfirmed.DataBind();
            dt.Clear();
            cmd.Dispose();
        }

        protected void btnApply_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(listUnitAvailable.SelectedValue))
            {
                Response.Write("<script>alert('Select a unit to apply');</script>");
                return;
            }

            if (_model.CheckUnitRequestExists(listUnitAvailable.SelectedValue, User.Identity.GetUserId()))
            {
                Response.Write("<script>alert('Unit already applied');</script>");
                return;
            }

            con.Open();
            cmd = new SqlCommand("dbo.InsertApplyUnits", con);
            cmd.Parameters.Add(new SqlParameter("@UserId", User.Identity.GetUserId()));
            cmd.Parameters.Add(new SqlParameter("@UnitId", listUnitAvailable.SelectedValue));
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
            LoadLists();
        }
    }
}