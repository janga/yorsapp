﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using YORS.Models;
using YORS.Roles;

namespace YORS.Unit
{
    public partial class ManageUnits : System.Web.UI.Page
    {
        private readonly UnitModels _model = new UnitModels();
        protected void Page_Load(object sender, EventArgs e)
        {           
            try
            {
                var dt = _model.GetUnits();
                var tableString = dt.AsEnumerable().Aggregate("<table><tr><td>Unit</td><td></td><td></td></tr>", (current, item) => current + "<tr><td>" + item[2]
                + "<td><td>" + "<a runat='server' href='EditUnit.aspx?id=" + item[0] + "'>Edit</a>" + "</td><td>" + "<a runat='server' href='DeleteUnit.aspx?id=" + item[0] + "'>Delete</a>" + "</td></tr>");

                tableString = tableString + "</table>";
                unitList.InnerHtml = tableString;
                CreateUnit.NavigateUrl = "CreateUnit";
                dt.Clear();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
        }
    }
}