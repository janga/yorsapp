﻿<%@ Page Title="Manage Unit" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageUnits.aspx.cs" Inherits="YORS.Unit.ManageUnits" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="unitList" runat="server"></div>
    <p>
        <asp:HyperLink runat="server" ID="CreateUnit" ViewStateMode="Disabled">Create Unit</asp:HyperLink>                   
    </p>
</asp:Content>
