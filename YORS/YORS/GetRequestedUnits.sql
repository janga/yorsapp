--Name : GetRequestedUnits
--Written by : Andrew Jang
--Purpose : Retrieve List of requested units
--Log
--Andrew Jang - Created - March 15th, 2017

use DB_110913_yors

if OBJECT_ID('GetRequestedUnits') IS NOT NULL
  begin
	drop procedure GetRequestedUnits
  end
go

Create procedure GetRequestedUnits
	@UserId as nvarchar(128)
as

begin
begin tran
SET NOCOUNT ON;

select u.*
from User_Unit_Request uur inner join Unit u on uur.UnitId = u.UnitId
where uur.UserId = @UserId

--rollback tran
commit tran
end
go