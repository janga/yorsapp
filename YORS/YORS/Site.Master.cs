﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;

namespace YORS
{
    public partial class SiteMaster : MasterPage
    {
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;
        private SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["YORSDBConnection"].ConnectionString);
        private SqlDataAdapter da = new SqlDataAdapter();
        private DataTable dt = new DataTable();
        private SqlCommand cmd = new SqlCommand();
        //public DropDownList passList => ddlSelectedUnit;
        //public event CommandEventHandler unitChanged;
        public string SelectedUnit
        {
            get { return ddlSelectedUnit.SelectedValue; }
            set { ddlSelectedUnit.SelectedValue = value; }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            // The code below helps to protect against XSRF attacks
            var requestCookie = Request.Cookies[AntiXsrfTokenKey];
            Guid requestCookieGuidValue;
            if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
            {
                // Use the Anti-XSRF token from the cookie
                _antiXsrfTokenValue = requestCookie.Value;
                Page.ViewStateUserKey = _antiXsrfTokenValue;
            }
            else
            {
                // Generate a new Anti-XSRF token and save to the cookie
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
                Page.ViewStateUserKey = _antiXsrfTokenValue;

                var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                {
                    HttpOnly = true,
                    Value = _antiXsrfTokenValue
                };
                if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                {
                    responseCookie.Secure = true;
                }
                Response.Cookies.Set(responseCookie);
            }
            //ViewState.Add("selectedUnit", ".");
            Session.Add("selectedUnit","");
            Page.PreLoad += master_Page_PreLoad;
        }

        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
            }
            else
            {
                // Validate the Anti-XSRF token
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                    || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;

            try
            {
                if (!Context.User.Identity.IsAuthenticated) return;
                con.Open();
                cmd = new SqlCommand("dbo.GetConfirmedUnits", con);
                cmd.Parameters.Add(new SqlParameter("@UserId", Context.User.Identity.GetUserId()));
                cmd.CommandType = CommandType.StoredProcedure;
                da.SelectCommand = cmd;
                da.Fill(dt);

                ddlSelectedUnit.DataSource = dt;
                ddlSelectedUnit.DataTextField = "UnitName";
                ddlSelectedUnit.DataValueField = "UnitId";
                ddlSelectedUnit.DataBind();

                ddlSelectedUnit.SelectedIndex = 0;
                if(ddlSelectedUnit.SelectedItem != null)
                    unitName.InnerText = ddlSelectedUnit.SelectedItem.Text;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
            }
        }

        protected void Unnamed_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            Context.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }

        protected void ddlSelectedUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("dbo.UpdateDefaultUnit", con);
                cmd.Parameters.Add(new SqlParameter("@UserId", Context.User.Identity.GetUserId()));
                cmd.Parameters.Add(new SqlParameter("@UnitId", ddlSelectedUnit.SelectedValue));
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
                
                unitName.InnerText = ddlSelectedUnit.SelectedItem.Text;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
            }
            
        }
    }

}